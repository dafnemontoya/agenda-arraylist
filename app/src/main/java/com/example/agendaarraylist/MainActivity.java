package com.example.agendaarraylist;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Contacto> contactos = new ArrayList<>();
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private Contacto savedContact;
    private int savedIndex;
    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = (EditText) findViewById(R.id.txtNombre);
        edtTelefono = (EditText) findViewById(R.id.txtTel1);
        edtTelefono2 = (EditText) findViewById(R.id.txtTel2);
        edtDireccion = (EditText) findViewById(R.id.txtDomicilio);
        edtNotas = (EditText) findViewById(R.id.txtNota);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btnSalir = (Button) findViewById(R.id.btnSalir);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtNombre.getText().toString().equals("") ||
                        edtDireccion.getText().toString().equals("") ||
                        edtTelefono.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, R.string.msgerror, Toast.LENGTH_SHORT).show();
                }
                else {
                    Contacto nContacto = new Contacto();

                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    nContacto.setFavorito(cbxFavorito.isChecked());

                    //Si ya había un contacto guardado || se presionó el botón Ver
                    if (savedContact != null) {
                        nContacto.setId(savedIndex);
                        for(int x=0; x < contactos.size(); x++) {
                                if ((int) contactos.get(x).getId() == savedIndex) {
                                    contactos.set(x, nContacto);
                                }
                        }
                        savedContact = null;
                    }
                    else {
                        int idx = index;
                        nContacto.setId(idx);
                        contactos.add(nContacto);
                        index++;
                    }

                    Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                    limpiar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //limpiar();
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                Bundle bObject = new Bundle();
                bObject.putSerializable("contactos", contactos);
                bObject.putInt("index", index);
                i.putExtras(bObject);
                startActivityForResult(i,0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    } //fin onCreate

    public void limpiar() {
        edtNombre.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtDireccion.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);
        savedContact = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //No le pico a nuevo, sino a Ver, por lo que trae datos
        if (data != null) {
            Bundle oBundle = data.getExtras();

            if (oBundle.getInt("index") > 0) {
                index = oBundle.getInt("index");
            }

            if(oBundle.getSerializable("contacto") instanceof Contacto)
            {
                savedContact = (Contacto) oBundle.getSerializable("contacto");
                savedIndex = oBundle.getInt("index");
                edtNombre.setText(savedContact.getNombre());
                edtTelefono.setText(savedContact.getTelefono1());
                edtTelefono2.setText(savedContact.getTelefono2());
                edtDireccion.setText(savedContact.getDomicilio());
                edtNotas.setText(savedContact.getNotas());
                cbxFavorito.setChecked(savedContact.isFavorito());
            }
            contactos = (ArrayList<Contacto>) oBundle.getSerializable("listaContactos");
            boolean nuevo = oBundle.getBoolean("nuevo");
            if(nuevo){
                limpiar();
            }
        }
    }

}
